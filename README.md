# themer-termite

[![Build Status](https://travis-ci.org/0x52a1/themer-termite.svg?branch=master)](https://travis-ci.org/0x52a1/themer-termite)

A [Termite][termite] theme template for [themer][themer].

## Installation and usage

```
npm install themer-termite
```

Then pass `themer-termite` as a template to themer when generating:

```
themer -c my-colors.js -t themer-termite -o gen
```

## Output

`themer-termite` will generate a file for each enabled colorscheme (light or dark), which will be in the output directory as `light.config` or `dark.config`.

Pick the one you want to use and copy it's contents into your `$HOME/.config/termite/config` file, removing any pre-existing `[colors]` sections.

## License

MIT, see [LICENSE](./LICENSE).

[termite]: https://github.com/thestinger/termite
[themer]: https://github.com/mjswensen/themer
